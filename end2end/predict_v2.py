import numpy as np
import torch
import os
import copy
from scipy.spatial.transform import Rotation
from torch.utils.data import DataLoader
import argparse

import sys
sys.path.append('../')
from speed_net.model import DualCNNLSTM as SpeedNet
from speed_net.dataset import GT1205Dataset as GT1205DatasetforSpeed
from speed_net.dataset import StrideSequenceDataset as StrideSequenceDatasetforSpeed

from heading_net.model import BiLSTM as HeadingNet
from heading_net.dataset import GT1205Dataset as GT1205DatasetforHeading
from heading_net.dataset import StrideSequenceDataset as StrideSequenceDatasetforHeading

from gravity_net.model import LSTM as GravityNet


def rotate_dcs_to_gcs_by_quat(dcs, quat):
    R_matrix = Rotation.from_quat(quat).as_matrix()
    gcs = copy.deepcopy(dcs)
    for i in range(len(R_matrix)):
        gcs[i, :] = np.dot(R_matrix[i], dcs[i, :])
    return gcs

def calc_horizontal_linacc(linacc, grav):
    hori_linacc = copy.deepcopy(linacc)
    for i in range(len(linacc)):
        hori_linacc[i, :] = linacc[i, :] - np.dot(linacc[i, :], grav[i, :])/np.dot(grav[i, :], grav[i, :]) * grav[i, :]
    return hori_linacc

def apply_lpf_gaussian(x, ts, sigma):
    sigma_k = sigma/(ts[1]-ts[0]) 
    kernel = np.zeros(int(round(3*sigma_k))*2+1)
    for i in range(kernel.shape[0]):
        kernel[i] =  1.0/np.sqrt(2*np.pi)/sigma_k * np.exp((i - round(3*sigma_k))**2/(- 2*sigma_k**2))
    kernel = kernel / kernel.sum()
    x_long = np.zeros(x.shape[0] + kernel.shape[0])
    x_long[kernel.shape[0]//2 :-kernel.shape[0]//2] = x
    x_long[:kernel.shape[0]//2 ] = x[0]
    x_long[-kernel.shape[0]//2 :] = x[-1]
    x_GC = np.convolve(x_long,kernel,'same')
    return x_GC[kernel.shape[0]//2 :-kernel.shape[0]//2]


def main(args):
    gpuid = 0
    device = 'cuda:' + str(gpuid) if torch.cuda.is_available() else 'cpu'

    with open("../lists/gt1205/test.txt", "r", encoding="utf-8") as f:
        lines = f.readlines()
        test_file_list = [line.rstrip('\n') for line in lines]
    test_data_path_list = [os.path.join(args.dataset_dir, file) for file in test_file_list]

    model_speed = SpeedNet(
        channel = 16, 
        kernel_size = 12, 
        hidden_size = 64, 
        layer_size = 1
    ).to(device)
    model_speed.load_state_dict(torch.load(args.speed_model))

    model_heading = HeadingNet(
        hidden_size=32, 
        num_layers=2, 
        dropout=0.5
    ).to(device)
    model_heading.load_state_dict(torch.load(args.heading_model))

    model_gravity = GravityNet(
        hidden_size=64, 
        num_layers=1,
        dropout=0.25
    ).to(device)
    model_gravity.load_state_dict(torch.load(args.gravity_model))


    for test_data_path in test_data_path_list:
        data = np.loadtxt(test_data_path, delimiter=',')
        ts = data[:, 0]
        acc = data[:, 1:4]
        gyro = data[:, 7:10]
        grav_api = data[:, 4:7]
        quat = data[:, 13:17]
        pos = data[:, 17:20]

        # Gravity estimation
        feature_gravity = torch.from_numpy(np.concatenate([acc, gyro], axis=1)[None, :, :].astype(np.float32)).to(device)
        hidden = torch.zeros(model_gravity.num_layers, 1, model_gravity.hidden_size).to(device)
        cell = torch.zeros(model_gravity.num_layers, 1, model_gravity.hidden_size).to(device)
        with torch.no_grad():
            model_gravity.eval()
            grav_, _, _ = model_gravity(feature_gravity, hidden, cell)
            grav = grav_[0, :, :].to('cpu').detach().numpy().copy()

        torch.save(model_gravity.state_dict(), './model.pth')
        # Feature for speed estimation
        norm_acc = np.linalg.norm(acc, axis=1, keepdims=True)
        feature_speed = np.concatenate([acc, norm_acc], axis=1)

        # Feature for heading estimation
        linacc = copy.deepcopy(acc-grav)
        ## Rotate coordinate system
        linacc_gcs = rotate_dcs_to_gcs_by_quat(linacc, quat)
        grav_gcs = rotate_dcs_to_gcs_by_quat(grav, quat)
        ## Extract horizontal components
        hori_linacc_gcs = calc_horizontal_linacc(linacc_gcs, grav_gcs)
        ## Gaussian filter
        hori_linacc_gcs_lpf_gaussian_x = apply_lpf_gaussian(hori_linacc_gcs[:, 0], ts, sigma=0.011)  # index 0 is x componet because z-axis is aligned with gravity
        hori_linacc_gcs_lpf_gaussian_y = apply_lpf_gaussian(hori_linacc_gcs[:, 1], ts, sigma=0.011)  # index 1 is y componet because z-axis is aligned with gravity
        feature_heading = np.concatenate([hori_linacc_gcs_lpf_gaussian_x[:, None], hori_linacc_gcs_lpf_gaussian_y[:, None]], axis=1)

        window_speed = 200
        window_heading = 400

        batch_feature_speed = []
        batch_feature_heading = []
        for i in range(0, feature_speed.shape[0] - max(window_speed, window_heading), 1):
            batch_feature_speed.append(feature_speed[i:i+window_speed, :])
            batch_feature_heading.append(feature_heading[i:i+window_heading, :])
        batch_feature_speed = torch.from_numpy(np.array(batch_feature_speed).astype(np.float32)).to(device)
        batch_feature_heading = torch.from_numpy(np.array(batch_feature_heading).astype(np.float32)).to(device)


        model_speed.eval()
        model_heading.eval()
        with torch.no_grad():
            hidden = (torch.ones(model_speed.layer_size, batch_feature_speed.size()[0], model_speed.hidden_size).to(device), torch.ones(model_speed.layer_size, batch_feature_speed.size()[0], model_speed.hidden_size).to(device))
            pred_speed = model_speed(batch_feature_speed, hidden).to('cpu').detach().numpy().copy()
            pred_heading = model_heading(batch_feature_heading).to('cpu').detach().numpy().copy()

        pred_vel = pred_speed[:, None] * pred_heading

        if not os.path.exists(os.path.join(args.result_dir)):
            os.makedirs(os.path.join(args.result_dir))
        np.savetxt(
            os.path.join(args.result_dir, os.path.basename(test_data_path)),
            pred_vel, 
            fmt=['%.5f', '%.5f'], delimiter=',')
        print("Result saved to {}".format(os.path.join(args.result_dir, os.path.basename(test_data_path))))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_dir', default='../gt1205_dataset/dataset', help='使用するデータセットのディレクトリのパス')
    parser.add_argument('--speed_model', default='../speed_net/pretrained_model/model.pth', help='学習済み速度推定モデルのパス')
    parser.add_argument('--heading_model', default='../heading_net/pretrained_model/model.pth', help='学習済み進行方向推定モデルのパス')
    parser.add_argument('--gravity_model', default='../gravity_net/pretrained_model/model.pth', help='学習済み重力方向推定モデルのパス')
    parser.add_argument('--result_dir', default='./predict_v2', help='結果を保存するディレクトリのパス')
    parser.add_argument('--test_list', default='../lists/gt1205/test.txt', help='評価データリストのパス')
    parser.add_argument('--gpuid', type=int, default=0, help='GPU ID')
    args = parser.parse_args()
    main(args)