import numpy as np
import torch
import os
from torch.utils.data import DataLoader
import argparse

import sys
sys.path.append('../')
from speed_net.model import DualCNNLSTM as SpeedNet
from speed_net.dataset import GT1205Dataset as GT1205DatasetforSpeed
from speed_net.dataset import StrideSequenceDataset as StrideSequenceDatasetforSpeed

from heading_net.model import BiLSTM as HeadingNet
from heading_net.dataset import GT1205Dataset as GT1205DatasetforHeading
from heading_net.dataset import StrideSequenceDataset as StrideSequenceDatasetforHeading



def main(args):
    device = 'cuda:' + str(args.gpuid) if torch.cuda.is_available() else 'cpu'

    with open(args.test_list, "r", encoding="utf-8") as f:
        lines = f.readlines()
        test_file_list = [line.rstrip('\n') for line in lines]
    test_data_path_list = [os.path.join(args.dataset_dir, file) for file in test_file_list]


    model_speed = SpeedNet(
        channel = 16, 
        kernel_size = 12, 
        hidden_size = 64, 
        layer_size = 1
    ).to(device)
    model_speed.load_state_dict(torch.load(args.speed_model))

    model_heading = HeadingNet(
        hidden_size=32, 
        num_layers=2, 
        dropout=0.5
    ).to(device)
    model_heading.load_state_dict(torch.load(args.heading_model))

    for test_data_path in test_data_path_list:
        test_dataset_speed = StrideSequenceDatasetforSpeed(
            data_path_list = [test_data_path], 
            dataset = GT1205DatasetforSpeed(), 
            window_size = 200, 
            stride_size = 1
        )
        test_data_loader_speed = DataLoader(
            test_dataset_speed, 
            batch_size=512, 
            shuffle=False, 
            num_workers=4
        )

        test_dataset_heading = StrideSequenceDatasetforHeading(
            data_path_list = [test_data_path], 
            dataset = GT1205DatasetforHeading(), 
            window_size = 400, 
            stride_size = 1
        )
        test_data_loader_heading = DataLoader(
            test_dataset_heading, 
            batch_size=512, 
            shuffle=False, 
            num_workers=4
        )
        with torch.no_grad():
            tmp_pred_speed = []
            tmp_targ_speed = []
            for i, (feature_speed, target_speed) in enumerate(test_data_loader_speed):
                feature_speed = feature_speed.to(device)
                hidden = (torch.ones(model_speed.layer_size, feature_speed.size()[0], model_speed.hidden_size).to(device), torch.ones(model_speed.layer_size, feature_speed.size()[0], model_speed.hidden_size).to(device))
                tmp_pred_speed.append(model_speed(feature_speed, hidden).to('cpu').detach().numpy().copy())
                tmp_targ_speed.append(target_speed.to('cpu').detach().numpy().copy())
            pred_speed = np.concatenate(tmp_pred_speed, axis=0)
            targ_speed = np.concatenate(tmp_targ_speed, axis=0)

            tmp_pred_heading = []
            tmp_targ_heading = []
            for i, (feature_heading, target_heading) in enumerate(test_data_loader_heading):
                feature_heading = feature_heading.to(device)
                tmp_pred_heading.append(model_heading(feature_heading).to('cpu').detach().numpy().copy())
                tmp_targ_heading.append(target_heading.to('cpu').detach().numpy().copy())
            pred_heading = np.concatenate(tmp_pred_heading, axis=0)
            targ_heading = np.concatenate(tmp_targ_heading, axis=0)

        pred_vel = pred_speed[:len(pred_heading), None] * pred_heading
        targ_vel = targ_speed[:len(targ_heading), None] * targ_heading

        if not os.path.exists(os.path.join(args.result_dir)):
            os.makedirs(os.path.join(args.result_dir))
        np.savetxt(
            os.path.join(args.result_dir, os.path.basename(test_data_path)),
            np.concatenate([pred_vel, targ_vel], axis=1), 
            fmt=['%.5f', '%.5f', '%.5f', '%.5f'], delimiter=',')
        print("Result saved to {}".format(os.path.join(args.result_dir, os.path.basename(test_data_path))))



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_dir', default='../gt1205_dataset/dataset', help='使用するデータセットのディレクトリのパス')
    parser.add_argument('--speed_model', default='../speed_net/pretrained_model/model.pth', help='学習済み速度推定モデルのパス')
    parser.add_argument('--heading_model', default='../heading_net/pretrained_model/model.pth', help='学習済み進行方向推定モデルのパス')
    parser.add_argument('--result_dir', default='./predict_v1', help='結果を保存するディレクトリのパス')
    parser.add_argument('--test_list', default='../lists/gt1205/test.txt', help='評価データリストのパス')
    parser.add_argument('--gpuid', type=int, default=0, help='GPU ID')
    args = parser.parse_args()
    main(args)