import numpy as np
import copy
import torch
from torch.utils.data import Dataset
from scipy.spatial.transform import Rotation

class GT1205Dataset():

    def __init__(self):
        self.freq = 100
        self.feature = None
        self.target = None

    def load(self, data_path):
        def rotate_dcs_to_gcs_by_quat(dcs, quat):
            R_matrix = Rotation.from_quat(quat).as_matrix()
            gcs = copy.deepcopy(dcs)
            for i in range(len(R_matrix)):
                gcs[i, :] = np.dot(R_matrix[i], dcs[i, :])
            return gcs

        def calc_horizontal_linacc(linacc, grav):
            hori_linacc = copy.deepcopy(linacc)
            for i in range(len(linacc)):
                hori_linacc[i, :] = linacc[i, :] - np.dot(linacc[i, :], grav[i, :])/np.dot(grav[i, :], grav[i, :]) * grav[i, :]
            return hori_linacc

        def apply_lpf_gaussian(x, ts, sigma):
            sigma_k = sigma/(ts[1]-ts[0]) 
            kernel = np.zeros(int(round(3*sigma_k))*2+1)
            for i in range(kernel.shape[0]):
                kernel[i] =  1.0/np.sqrt(2*np.pi)/sigma_k * np.exp((i - round(3*sigma_k))**2/(- 2*sigma_k**2))
            kernel = kernel / kernel.sum()
            x_long = np.zeros(x.shape[0] + kernel.shape[0])
            x_long[kernel.shape[0]//2 :-kernel.shape[0]//2] = x
            x_long[:kernel.shape[0]//2 ] = x[0]
            x_long[-kernel.shape[0]//2 :] = x[-1]
            x_GC = np.convolve(x_long,kernel,'same')
            return x_GC[kernel.shape[0]//2 :-kernel.shape[0]//2]

        def calc_vel_from_pos(pos, diff_size=10):
            vel = np.array([pos[i]-pos[i-diff_size] for i in range(diff_size, len(pos))]) * self.freq / diff_size
            return vel

        data = np.loadtxt(data_path, delimiter=',')
        ts = data[:, 0]
        acc = data[:, 1:4]
        grav = data[:, 4:7]
        quat = data[:, 13:17]
        pos = data[:, 17:20]
        linacc = copy.deepcopy(acc-grav)
        # Rotate coordinate system
        linacc_gcs = rotate_dcs_to_gcs_by_quat(linacc, quat)
        grav_gcs = rotate_dcs_to_gcs_by_quat(grav, quat)
        # Extract horizontal components
        hori_linacc_gcs = calc_horizontal_linacc(linacc_gcs, grav_gcs)
        # Gaussian filter
        hori_linacc_gcs_lpf_gaussian_x = apply_lpf_gaussian(hori_linacc_gcs[:, 0], ts, sigma=0.011)  # index 0 is x componet because z-axis is aligned with gravity
        hori_linacc_gcs_lpf_gaussian_y = apply_lpf_gaussian(hori_linacc_gcs[:, 1], ts, sigma=0.011)  # index 1 is y componet because z-axis is aligned with gravity
        self.feature = np.concatenate([hori_linacc_gcs_lpf_gaussian_x[:, None], hori_linacc_gcs_lpf_gaussian_y[:, None]], axis=1)
        # Ground truth
        vel = calc_vel_from_pos(pos, diff_size=10)
        hori_vel = vel[:, [0,1]] # The coordinate system of vel (pos) and GCS are matched in advance.
        self.target = hori_vel / np.linalg.norm(hori_vel, axis=1, keepdims=True)

    def get_feature(self):
        return self.feature

    def get_target(self):
        return self.target


class StrideSequenceDataset(Dataset):

    def __init__(self, data_path_list, dataset, window_size, stride_size):
        self.data_path_list = data_path_list
        self.dataset = dataset
        self.window_size = window_size
        self.stride_size = stride_size
        self.feature_list = []
        self.target_list = []
        self.index_map = []
        self.preprocess()

    def __len__(self):
        return len(self.index_map)

    def __getitem__(self, idx):
        seq_id, frame_id = self.index_map[idx][0], self.index_map[idx][1]
        feature = torch.from_numpy(self.feature_list[seq_id][frame_id-self.window_size:frame_id].astype(np.float32))
        target = torch.mean(torch.from_numpy(self.target_list[seq_id][frame_id-self.window_size:frame_id].astype(np.float32)), dim=0)
        return feature, target

    def preprocess(self):
        for data_path in self.data_path_list:
            self.dataset.load(data_path)
            self.feature_list.append(self.dataset.get_feature())
            self.target_list.append(self.dataset.get_target())

        for i in range(len(self.target_list)):
            self.index_map += [[i, j] for j in range(self.window_size, len(self.target_list[i]), self.stride_size)]