import torch
import torch.nn as nn
import torch.nn.functional as F


class BiLSTM(nn.Module):
    
    # define network params by using constructor
    def __init__(self, hidden_size, num_layers, dropout):
        # super class constructor
        super(BiLSTM, self).__init__()
        self.input_size = 2
        self.output_size = 2
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.dropout = dropout
        
        if self.num_layers == 1:
            self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers, bidirectional=True, batch_first=True)
        else:
            self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers, dropout=self.dropout, bidirectional=True, batch_first=True)
        self.linear = nn.Linear(self.hidden_size*2, self.output_size)
        self.bn = nn.BatchNorm1d(self.hidden_size*2)

        
    def forward(self, feature):
        x, _ = self.lstm(feature)  # (N,L,Hin) when batch_first=True
        x = torch.cat((x[:, -1, :self.hidden_size], x[:, 0, self.hidden_size:]),dim=1) # (N, L, D * H_{out})(N,L,D∗Hout) when batch_first=True
        x = F.relu(x)
        x = self.linear(x)
        output = x/torch.norm(x, dim=1, keepdim=True)
        return output


class CNN(nn.Module):
    
    # define network params by using constructor
    def __init__(self, window, k1, k2, s1, s2, out_ch1, out_ch2, hidden_size):
        
        # super class constructor
        super(CNN, self).__init__()
        
        self.input_size = window
        self.output_size = 2
        
        self.k1 = k1
        self.s1 = s1
        self.in_ch1 = 2
        self.out_ch1 = out_ch1
        
        self.k2 = k2
        self.s2 = s2
        self.in_ch2 = out_ch1
        self.out_ch2 = out_ch2

        self.hidden_size1 = int((( int((self.input_size - self.k1) / self.s1 + 1) - self.k2) / self.s2 + 1) * self.out_ch2)
        self.hidden_size2 = hidden_size
        
        self.conv1 = nn.Conv1d(self.in_ch1, self.out_ch1, self.k1, self.s1)
        self.conv2 = nn.Conv1d(self.in_ch2, self.out_ch2, self.k2, self.s2)
        
        self.bn1 = nn.BatchNorm1d(self.out_ch1)
        self.bn2 = nn.BatchNorm1d(self.out_ch2)
        
        self.linear1 = nn.Linear(self.hidden_size1, self.hidden_size2)
        self.linear2 = nn.Linear(self.hidden_size2, self.output_size)

        
    def forward(self, feature):
        x = feature.transpose(1, 2).contiguous()
        x= self.conv1(x)
        x = self.bn1(x)
        x = F.relu(x)
        x= self.conv2(x)
        x = self.bn2(x)
        x = F.relu(x)
        x = torch.flatten(x, start_dim=1)
        x = self.linear1(x)
        x = F.relu(x)
        x = self.linear2(x)
        output = x/torch.norm(x, dim=1, keepdim=True)
        
        return output