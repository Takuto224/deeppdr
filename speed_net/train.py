import os
import json
import torch
from torch import optim, nn
from torch.utils.data import DataLoader
import argparse 

from dataset import StrideSequenceDataset, GT1205Dataset
from model import DualCNNLSTM


def train(epoch, model, criterion, optimizer, train_data_loader, device):
    model.train()
    epoch_loss = 0
    for iteration, (feature, target) in enumerate(train_data_loader):
        feature, target = feature.to(device), target.to(device)
        optimizer.zero_grad()
        hidden = (torch.ones(model.layer_size, feature.size()[0], model.hidden_size).to(device), torch.ones(model.layer_size, feature.size()[0], model.hidden_size).to(device))
        loss = criterion(model(feature, hidden), target)
        epoch_loss += loss.item()
        loss.backward()
        optimizer.step()
        # print("===> Epoch[{}]({}/{}): Loss: {:.4f}".format(epoch, iteration, len(train_data_loader), loss.item()))
    print("===> Epoch {} Complete: Avg. Train Loss: {:.4f}".format(epoch, epoch_loss / len(train_data_loader)))
    return epoch_loss / len(train_data_loader)

def val(epoch, model, criterion, optimizer, val_data_loader, device):
    model.eval()
    epoch_loss = 0
    with torch.no_grad():
        for iteration, (feature, target) in enumerate(val_data_loader):
            feature, target = feature.to(device), target.to(device)
            hidden = (torch.ones(model.layer_size, feature.size()[0], model.hidden_size).to(device), torch.ones(model.layer_size, feature.size()[0], model.hidden_size).to(device))
            loss = criterion(model(feature, hidden), target)
            epoch_loss += loss.item()
            # print("===> Epoch[{}]({}/{}): Loss: {:.4f}".format(epoch, iteration, len(val_data_loader), loss.item()))
    print("===> Epoch {} Complete: Avg. Val Loss: {:.4f}".format(epoch, epoch_loss / len(val_data_loader)))
    return epoch_loss / len(val_data_loader)

def checkpoint(epoch, model, result_dir):
    model_save_path = os.path.join(result_dir, "model_epoch_{}.pth".format(epoch))
    torch.save(model.state_dict(), model_save_path)
    print("Checkpoint saved to {}".format(model_save_path))

def main(args):
    device = 'cuda:' + str(args.gpuid) if torch.cuda.is_available() else 'cpu'

    with open(args.train_list, "r", encoding="utf-8") as f:
        lines = f.readlines()
        train_file_list = [line.rstrip('\n') for line in lines]
    train_data_path_list = [os.path.join(args.dataset_dir, file) for file in train_file_list]

    with open(args.val_list, "r", encoding="utf-8") as f:
        lines = f.readlines()
        val_file_list = [line.rstrip('\n') for line in lines]
    val_data_path_list = [os.path.join(args.dataset_dir, file) for file in val_file_list]

    train_dataset = StrideSequenceDataset(
        data_path_list = train_data_path_list, 
        dataset = GT1205Dataset(), 
        window_size = 200, 
        stride_size = 100
    )
    val_dataset = StrideSequenceDataset(
        data_path_list = val_data_path_list, 
        dataset = GT1205Dataset(), 
        window_size = 200, 
        stride_size = 100
    )

    train_data_loader = DataLoader(
        train_dataset, 
        batch_size=512, 
        shuffle=True, 
        num_workers=4
    )
    val_data_loader = DataLoader(
        val_dataset, 
        batch_size=512, 
        shuffle=True, 
        num_workers=4
    )

    model = DualCNNLSTM(
        channel = 16, 
        kernel_size = 12, 
        hidden_size = 64, 
        layer_size = 1
    ).to(device)

    criterion = nn.MSELoss()
    optimizer = optim.Adam(model.parameters(), lr=0.003)
    loss_log = {
        'train_loss_log': [],
        'val_loss_log': []
    }
    if not os.path.exists(os.path.join(args.result_dir)):
        os.makedirs(os.path.join(args.result_dir))
    for epoch in range(1, args.epoch+1):
        loss_log['train_loss_log'].append(train(epoch, model, criterion, optimizer, train_data_loader, device))
        loss_log['val_loss_log'].append(val(epoch, model, criterion, optimizer, val_data_loader, device))
        checkpoint(epoch, model, args.result_dir)
    with open(os.path.join(args.result_dir, 'loss.json'), 'w') as f:
        json.dump(loss_log, f, indent=2, ensure_ascii=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_dir', default='../gt1205_dataset/dataset', help='使用するデータセットのディレクトリのパス')
    parser.add_argument('--result_dir', default='./result', help='結果を保存するディレクトリのパス')
    parser.add_argument('--train_list', default='../lists/gt1205/train.txt', help='訓練データリストのパス')
    parser.add_argument('--val_list', default='../lists/gt1205/train.txt', help='検証データリストのパス')
    parser.add_argument('--gpuid', type=int, default=0, help='GPU ID')
    parser.add_argument('--epoch', type=int, default=100, help='学習エポック数')
    args = parser.parse_args()
    main(args)