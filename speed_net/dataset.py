import numpy as np
import copy
import torch
from torch.utils.data import Dataset


class GT1205Dataset():

    def __init__(self):
        self.freq = 100
        self.feature = None
        self.target = None

    def load(self, data_path):
        def calc_vel_from_pos(pos):
            vel = np.diff(pos, axis=0) * self.freq
            return vel

        def calc_horizontal_vel_norm(vel, grav):
            hori_vel = copy.deepcopy(vel)
            for i in range(len(vel)):
                hori_vel[i, :] = vel[i, :] - (np.inner(grav[i, :], vel[i, :]) / sum(grav[i, :] ** 2)) * grav[i, :]
            hori_vel_norm = np.linalg.norm(hori_vel, axis=1)
            return hori_vel_norm

        data = np.loadtxt(data_path, delimiter=',')
        acc = data[:, 1:4]
        norm_acc = np.linalg.norm(acc, axis=1, keepdims=True)
        self.feature = np.concatenate([acc, norm_acc], axis=1)

        grav = data[:, 4:7]
        pos = data[:, 17:20]
        vel = calc_vel_from_pos(pos)
        self.target = calc_horizontal_vel_norm(vel, grav)

    def get_feature(self):
        return self.feature

    def get_target(self):
        return self.target


class StrideSequenceDataset(Dataset):

    def __init__(self, data_path_list, dataset, window_size, stride_size):
        self.data_path_list = data_path_list
        self.dataset = dataset
        self.window_size = window_size
        self.stride_size = stride_size
        self.feature_list = []
        self.target_list = []
        self.index_map = []
        self.preprocess()

    def __len__(self):
        return len(self.index_map)

    def __getitem__(self, idx):
        seq_id, frame_id = self.index_map[idx][0], self.index_map[idx][1]
        feature = torch.from_numpy(self.feature_list[seq_id][frame_id-self.window_size:frame_id].astype(np.float32))
        target = torch.mean(torch.from_numpy(self.target_list[seq_id][frame_id-self.window_size:frame_id].astype(np.float32)))
        return feature, target

    def preprocess(self):
        for data_path in self.data_path_list:
            self.dataset.load(data_path)
            self.feature_list.append(self.dataset.get_feature())
            self.target_list.append(self.dataset.get_target())

        for i in range(len(self.target_list)):
            self.index_map += [[i, j] for j in range(self.window_size, len(self.target_list[i]), self.stride_size)]