import torch
import torch.nn as nn
import torch.nn.functional as F


class DualCNNLSTM(nn.Module):
    def __init__(self, channel, kernel_size, hidden_size, layer_size):
        super(DualCNNLSTM, self).__init__()
        self.dim_in = 4
        self.channel = channel
        self.kernel_size = kernel_size
        self.hidden_size = hidden_size
        self.layer_size = layer_size

        # for convLSTM imu_input
        self.convl1 = nn.Conv1d(1, self.channel, self.kernel_size)
        self.convl1_drop = nn.Dropout(p=0.5)
        self.convl21 = nn.Conv1d(self.channel, self.channel, self.kernel_size)
        self.convl22 = nn.Conv1d(self.channel, self.channel, self.kernel_size)
        self.convl31 = nn.Conv1d(self.channel, self.channel, 2 * self.kernel_size)
        self.convl32 = nn.Conv1d(self.channel, self.channel, 2 * self.kernel_size)
        self.convl21_drop = nn.Dropout(p=0.5)
        self.convl22_drop = nn.Dropout(p=0.5)
        self.convl31_drop = nn.Dropout(p=0.5)
        self.convl32_drop = nn.Dropout(p=0.5)

        # for lstm mixed feature
        self.mlstm = nn.LSTM(2 * self.dim_in * self.channel, self.hidden_size, self.layer_size)

        self.output_vel = nn.Sequential(
            nn.Linear(self.hidden_size, 2 * self.hidden_size),
            nn.BatchNorm1d(2 * self.hidden_size),
            nn.ReLU(),
            nn.Linear(2 * self.hidden_size, 1),
            nn.ReLU()
        )

    def forward(self, feature, hidden):
        # conv lstm
        clstm_input = feature.transpose(1, 2).contiguous()
        clstm_input = torch.split(clstm_input, 1, dim=1)
        mid1 = []
        mid2 = []
        for axis in clstm_input:
            axis_mid = F.relu(self.convl1_drop(self.convl1(axis)))
            axis_out1 = F.relu(self.convl21_drop(self.convl21(axis_mid)))
            axis_out1 = F.relu(self.convl22_drop(self.convl22(axis_out1)))
            mid1.append(axis_out1)
            axis_out2 = F.relu(self.convl31_drop(self.convl31(axis_mid)))
            axis_out2 = F.relu(self.convl32_drop(self.convl32(axis_out2)))
            mid2.append(axis_out2)

        mid1 = torch.cat(mid1, dim=1).transpose(1, 2).transpose(0, 1).contiguous()
        mid2 = torch.cat(mid2, dim=1).transpose(1, 2).transpose(0, 1).contiguous()

        # lstm after mix
        mlstm_input = torch.cat([mid1[-1 * mid2.size(0):, :, :], mid2], dim=2)
        mlstm_out, hidden = self.mlstm(mlstm_input, hidden)
        mlstm_out = mlstm_out.transpose(0, 1).contiguous()

        mix = mlstm_out[:, -1, :]
        output = self.output_vel(mix)[:, 0]

        return output