from torch import nn

class LSTM(nn.Module):
    # define network params by using constructor
    def __init__(self, hidden_size, num_layers, dropout):
        # super class constructor
        super(LSTM, self).__init__()
        self.input_size = 6
        self.output_size = 3
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.dp = dropout

        self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size, num_layers=self.num_layers, bidirectional=False)
        self.dropout = nn.Dropout(p=self.dp)
        self.linear = nn.Linear(self.hidden_size, self.output_size)

    def forward(self, features, hidden, cell):
        features = features.transpose(0, 1).contiguous()
        seq_features, (hidden, cell) = self.lstm(features, (hidden, cell))
        seq_features_dp = self.dropout(seq_features.transpose(0, 1).contiguous())
        output = self.linear(seq_features_dp)
        return output, hidden, cell