import numpy as np
import copy
import math
import torch
from torch.utils.data import Dataset
from scipy.spatial.transform import Rotation


class GT1205Dataset():

    def __init__(self):
        self.freq = 100
        self.feature = None
        self.target = None

    def load(self, data_path):
        def calc_vel_from_pos(pos, diff_size=10):
            vel = np.array([pos[i]-pos[i-diff_size] for i in range(diff_size, len(pos))]) * self.freq / diff_size
            return vel

        def convert_xy_to_rad(X, Y):
            rad = np.array([math.atan2(y,x) for y, x in zip(Y,X)])
            return rad

        data = np.loadtxt(data_path, delimiter=',')
        acc = data[:, 1:4]
        gyro = data[:, 7:10]
        quat = data[:, 13:17]
        pos = data[:, 17:20]
        # Feature
        self.feature = np.concatenate([acc, gyro], axis=1)
        # Ground truth
        vel = calc_vel_from_pos(pos, diff_size=10)
        hori_vel = vel[:, [0,1]] # The coordinate system of vel (pos) and GCS are matched in advance.
        rad = convert_xy_to_rad(hori_vel[:, 0], hori_vel[:,1])
        self.target = rad

    def get_feature(self):
        return self.feature

    def get_target(self):
        return self.target


class StrideSequenceDataset(Dataset):

    def __init__(self, data_path_list, dataset, window_size, stride_size):
        self.data_path_list = data_path_list
        self.dataset = dataset
        self.window_size = window_size
        self.stride_size = stride_size
        self.feature_list = []
        self.target_list = []
        self.index_map = []
        self.preprocess()

    def __len__(self):
        return len(self.index_map)

    def __getitem__(self, idx):
        seq_id, frame_id = self.index_map[idx][0], self.index_map[idx][1]
        feature = self.feature_list[seq_id][frame_id-self.window_size:frame_id].astype(np.float32)
        rad_diff = self.target_list[seq_id][frame_id] - self.target_list[seq_id][frame_id-self.window_size]
        if rad_diff > np.pi:
            rad_diff -= 2*np.pi
        if rad_diff < -np.pi:
            rad_diff += 2*np.pi
        target = rad_diff.astype(np.float32)
        return seq_id, feature, target

    def preprocess(self):
        for data_path in self.data_path_list:
            self.dataset.load(data_path)
            self.feature_list.append(self.dataset.get_feature())
            self.target_list.append(self.dataset.get_target())

        for i in range(len(self.target_list)):
            self.index_map += [[i, j] for j in range(self.window_size, len(self.target_list[i]), self.stride_size)]