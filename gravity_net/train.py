import os
import json
import torch
from torch import optim, nn
from torch.utils.data import DataLoader
import argparse 

from dataset import StrideSequenceDataset, GT1205Dataset
from model import LSTM


class RGLoss(nn.Module):
    def __init__(self, k):
        super(RGLoss, self).__init__()
        self.k = k
        self.mse = nn.MSELoss()

    def forward(self, pred_h, gt_h, pred_grav, acc):
        loss = self.mse(pred_h, gt_h) + self.k*self.mse(pred_grav, acc)
        return loss

def train(epoch, model, criterion, optimizer, batch_size, train_data_loader, device):
    model.train()
    epoch_loss = 0
    pre_seq_id = -1
    loss = 0
    iter_num = 0
    for iteration, (seq_id, feature, target) in enumerate(train_data_loader):
        if seq_id.data.item() !=pre_seq_id:
            hidden = torch.zeros(model.num_layers, 1, model.hidden_size).to(device)
            cell = torch.zeros(model.num_layers, 1, model.hidden_size).to(device)
            pre_seq_id = seq_id.data.item()
        feature, target = feature.to(device), target.to(device)
        hidden, cell = hidden.detach(), cell.detach()
        optimizer.zero_grad()
        pred_grav, hidden, cell = model(feature, hidden, cell)
        pred_heading = torch.sum((-pred_grav[0, :, :] * feature[0, :, [3, 4, 5]]).sum(dim=1)) / 100
        loss = loss + criterion(pred_heading, target[0], pred_grav[0, :, :], feature[0, :, [0, 1, 2]])
        iter_num += 1
        # batch
        if iter_num % batch_size == 0 or len(train_data_loader) - iter_num == 0:
            epoch_loss += loss.data.item()
            optimizer.zero_grad()
            loss = loss/batch_size
            loss.backward()
            optimizer.step()
            loss = 0
        # print("===> Epoch[{}]({}/{}): Loss: {:.4f}".format(epoch, iteration, len(train_data_loader), loss.item()))
    print("===> Epoch {} Complete: Avg. Train Loss: {:.4f}".format(epoch, epoch_loss / len(train_data_loader)))
    return epoch_loss / len(train_data_loader)

def val(epoch, model, criterion, optimizer, batch_size, val_data_loader, device):
    model.eval()
    epoch_loss = 0
    pre_seq_id = -1
    loss = 0
    iter_num = 0
    with torch.no_grad():
        for iteration, (seq_id, feature, target) in enumerate(val_data_loader):
            if seq_id.data.item() !=pre_seq_id:
                hidden = torch.zeros(model.num_layers, 1, model.hidden_size).to(device)
                cell = torch.zeros(model.num_layers, 1, model.hidden_size).to(device)
                pre_seq_id = seq_id.data.item()
            feature, target = feature.to(device), target.to(device)
            hidden, cell = hidden.detach(), cell.detach()
            optimizer.zero_grad()
            pred_grav, hidden, cell = model(feature, hidden, cell)
            pred_heading = torch.sum((-pred_grav[0, :, :] * feature[0, :, [3, 4, 5]]).sum(dim=1)) / 100
            loss = loss + criterion(pred_heading, target[0], pred_grav[0, :, :], feature[0, :, [0, 1, 2]])
            iter_num += 1
            # batch
            if iter_num % batch_size == 0 or len(val_data_loader) - iter_num == 0:
                epoch_loss += loss.data.item()
                optimizer.step()
                loss = 0
        print("===> Epoch {} Complete: Avg. Train Loss: {:.4f}".format(epoch, epoch_loss / len(val_data_loader)))
    return epoch_loss / len(val_data_loader)

def checkpoint(epoch, model, result_dir):
    model_save_path = os.path.join(result_dir, "model_epoch_{}.pth".format(epoch))
    torch.save(model.state_dict(), model_save_path)
    print("Checkpoint saved to {}".format(model_save_path))

def main(args):
    device = 'cuda:' + str(args.gpuid) if torch.cuda.is_available() else 'cpu'

    with open(args.train_list, "r", encoding="utf-8") as f:
        lines = f.readlines()
        train_file_list = [line.rstrip('\n') for line in lines]
    train_data_path_list = [os.path.join(args.dataset_dir, file) for file in train_file_list]

    with open(args.val_list, "r", encoding="utf-8") as f:
        lines = f.readlines()
        val_file_list = [line.rstrip('\n') for line in lines]
    val_data_path_list = [os.path.join(args.dataset_dir, file) for file in val_file_list]

    train_dataset = StrideSequenceDataset(
        data_path_list = train_data_path_list, 
        dataset = GT1205Dataset(), 
        window_size = 800, 
        stride_size = 800
    )
    val_dataset = StrideSequenceDataset(
        data_path_list = val_data_path_list, 
        dataset = GT1205Dataset(), 
        window_size = 800, 
        stride_size = 800
    )

    train_data_loader = DataLoader(
        train_dataset, 
        batch_size=1, 
        shuffle=False, 
        num_workers=4
    )
    val_data_loader = DataLoader(
        val_dataset, 
        batch_size=1, 
        shuffle=False, 
        num_workers=4
    )

    model = LSTM(
        hidden_size=64, 
        num_layers=1,
        dropout=0.25
    ).to(device)

    criterion = criterion = RGLoss(k=0.1)
    optimizer = optim.Adam(model.parameters(), lr=0.001)
    batch_size = 512
    loss_log = {
        'train_loss_log': [],
        'val_loss_log': []
    }
    if not os.path.exists(os.path.join(args.result_dir)):
        os.makedirs(os.path.join(args.result_dir))
    for epoch in range(1, args.epoch+1):
        loss_log['train_loss_log'].append(train(epoch, model, criterion, optimizer, batch_size, train_data_loader, device))
        loss_log['val_loss_log'].append(val(epoch, model, criterion, optimizer, batch_size, val_data_loader, device))
        checkpoint(epoch, model, args.result_dir)
    with open(os.path.join(args.result_dir, 'loss.json'), 'w') as f:
        json.dump(loss_log, f, indent=2, ensure_ascii=False)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset_dir', default='../gt1205_dataset/dataset', help='使用するデータセットのディレクトリのパスを指定')
    parser.add_argument('--result_dir', default='./result', help='結果を保存するディレクトリのパスを指定')
    parser.add_argument('--train_list', default='../lists/gt1205/train.txt', help='訓練データリストのパス')
    parser.add_argument('--val_list', default='../lists/gt1205/train.txt', help='検証データリストのパス')
    parser.add_argument('--gpuid', type=int, default=0, help='GPU IDを指定')
    parser.add_argument('--epoch', type=int, default=500, help='学習エポック数を指定')
    args = parser.parse_args()
    main(args)