# DeepPDR
https://uclab.esa.io/posts/4792

# データセット
サンプルコードでは`gt1205_dataset`を使用する。研究室の共有ストレージ[QUAMO](https://uclab.esa.io/posts/113)の`192.168.207.80/uclab/00_students/takuto/dataset/gt1205_dataset/dataset`に保存してある。ダウンロードして本リポジトリのルートディレクトリに配置する。


||センサー|単位|説明|
|---|---|---|---|
|0 |Timestamp                     |s    |UNIX time               |
|1 |Acceleration (x-axis)          |g    |x軸方向の加速力（重量を含む）|
|2 |Acceleration (y-axis)          |g    |y軸方向の加速力（重量を含む）|
|3 |Acceleration (z-axis)          |g    |z軸方向の加速力（重量を含む）|
|4 |Gravity (x-axis)              |g    |x軸方向の重力加速度|
|5 |Gravity (y-axis)              |g    |y軸方向の重力加速度|
|6 |Gravity (z-axis)              |g    |z軸方向の重力加速度|
|7 |Angular velocity (x-axis)     |rad/s|x軸周りの角速度|
|8 |Angular velocity (y-axis)     |rad/s|y軸周りの角速度|
|9 |Angular velocity (z-axis)     |rad/s|z軸周りの角速度|
|10|Geomagnetic field (x-axis)    |μT   |x軸方向の地磁気強度|
|11|Geomagnetic field (y-axis)    |μT   |y軸方向の地磁気強度|
|12|Geomagnetic field (z-axis)    |μT   |z軸方向の地磁気強度|
|13|Rotation vector (x * sin(θ/2))|     |x軸に関する回転ベクトル成分|
|14|Rotation vector (y * sin(θ/2))|     |y軸に関する回転ベクトル成分|
|15|Rotation vector (z * sin(θ/2))|     |z軸に関する回転ベクトル成分|
|16|Rotation vector ((cos(θ/2))   |     |回転ベクトルのスカラー成分|
|17|Position (x-axis)             |m    |x軸方向の座標|
|18|Position (y-axis)             |m    |y軸方向の座標|
|19|Position (z-axis)             |m    |z軸方向の座標|

データは全て100Hzにリサンプリングしてある。`Rotation vector`と`Position`の基準座標系は[Android Motion SensorsのTYPE_ROTATION_VECTOR](https://developer.android.com/guide/topics/sensors/sensors_motion#sensors-motion-rotate)の定義と同じで、`Position`の初期位置は原点である。

# 学習

- `--dataset_dir`: 使用するデータセットのディレクトリのパス
- `--result_dir`: 結果を保存するディレクトリのパス
- `--train_list`: 訓練データリストのパス
- `--val_list`: 検証データリストのパス
- `--gpuid`: GPU ID
- `--epoch`: 学習エポック数

速度推定モデルを学習（引数はデフォルト値）

```shell
cd speed_net
python train.py \
--dataset_dir ../gt1205_dataset/dataset \
--result_dir ./result \
--train_list ../lists/gt1205/train.txt \
--val_list ../lists/gt1205/val.txt \
--gpuid 0 \
--epoch 100
```

重力方向推定モデルを学習（引数はデフォルト値）

```shell
cd gravity_net
python train.py \
--dataset_dir ../gt1205_dataset/dataset \
--result_dir ./result \
--train_list ../lists/gt1205/train.txt \
--val_list ../lists/gt1205/val.txt \
--gpuid 0 \
--epoch 500
```

進行方向推定モデルを学習（引数はデフォルト値）

```shell
cd heading_net
python train.py \
--dataset_dir ../gt1205_dataset/dataset \
--result_dir ./result \
--train_list ../lists/gt1205/train.txt \
--val_list ../lists/gt1205/val.txt \
--gpuid 0 \
--epoch 500
```

# 推論
- `--dataset_dir`: 使用するデータセットのディレクトリのパス
- `--speed_model`: 学習済み速度推定モデルのパス
- `--heading_model`: 学習済み進行方向推定モデルのパス
- `--gravity_model`: 学習済み重力方向推定モデルのパス
- `--result_dir`: 結果を保存するディレクトリのパス
- `--test_list`: 評価データリストのパス
- `--gpuid`: GPU ID


速度推定モデルと進行方向推定モデルを用いたEnd-to-EndでPDR

```shell
cd end2end
python predict_v1.py \
--dataset_dir ../gt1205_dataset/dataset \
--speed_model ../speed_net/pretrained_model/model.pth \
--heading_model ../heading_net/pretrained_model/model.pth \
--result_dir ./predict_v1 \
--test_list ../lists/gt1205/test.txt \
--gpuid 0
```

速度推定モデルと進行方向推定モデルと重力方向推定モデルを用いたEnd-to-EndでPDR

```shell
cd end2end
python predict_v2.py \
--dataset_dir ../gt1205_dataset/dataset \
--speed_model ../speed_net/pretrained_model/model.pth \
--heading_model ../heading_net/pretrained_model/model.pth \
--gravity_model ../gravity_net/pretrained_model/model.pth \
--result_dir ./predict_v2 \
--test_list ../lists/gt1205/test.txt \
--gpuid 0
```